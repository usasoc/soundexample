package com.example.soundexample;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collections;
import java.util.List;

import android.content.Context;
import android.media.MediaPlayer;
import android.view.View;
import android.widget.AdapterView;
import android.widget.AdapterView.OnItemSelectedListener;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.LinearLayout;
import android.widget.Spinner;
import android.widget.TextView;

public class MyView extends LinearLayout {
	private MediaPlayer mediaPlayer;

	public MyView(final Context context) {
		super(context);
		// TODO Auto-generated constructor stub
		final String[] names = {"Act Naturally",
                "Africa",
                "Al Bundy",
                "Alf",
                "All of Me",
                "The Andy Griffith Show Theme",
                "Another Brick in the Wall",
                "As Time Goes By",
                "All Through the Night",
                "Alone Again, Naturally",
                "At 17",
                "Bad Moon Rising",
                "Take Me Out to the Ballgame",
                "Beverly Hills Cop",
                "Bingo",
                "Happy Birthday",
                "Blowin in the Wind",
                "Break My Stride",
                "California, Here I Come",
                "Caisson",
                "I Just Called To Say I Love You",
                "Charlie Brown",
                "Cheers Theme",
                "Come On, Eileen",
                "Conga",
                "Dallas",
                "Danger Zone",
                "Do You Remember These",
                "The Entertainer",
                "Eternal Flame",
                "Fame",
                "Final Jeopardy",
                "Where Have All the Flowers Gone",
                "Ghost Riders In the Sky",
                "Grand Old Flag",
                "We're Happy Together",
                "Happy Trails",
                "Hawaii 50 Theme",
                "Hetty Wainthrop Investigates Theme",
                "Hill Street Blues",
                "Home, Home on the Range",
                "I Love Lucy Theme",
                "I'm So Excited",
                "You're the Inspiration",
                "It's All Coming Back to Me Now",
                "Jambalya",
                "Jeopardy Theme",
                "Keeping Up Appearances Theme",
                "King of the Road",
                "Knots Landing Theme",
                "Lean On Me",
                "The Lion Sleeps Tonight",
                "Lone Ranger Theme",
                "Longer Than",
                "Love Boat Theme",
                "Love and Marriage",
                "Lucille",
                "Macarena",
                "Marple Theme",
                "Mash Theme",
                "Memories",
                "Ferry Cross the Mersey",
                "Mission Impossible Theme",
                "Mockingbird Song",
                "Moon River",
                "Muppet Show Theme Song",
                "New York, New York",
                "Off We Go Into the Wild Blue Yonder",
                "An Okie From Muskogee",
                "Old Flame",
                "Old McDonald",
                "Paper Roses",
                "Perry Mason Theme",
                "Piano Man",
                "Picket Fences Theme",
                "Poirot Theme",
                "Popeye",
                "Puff the Magic Dragon",
                "Quincy Theme",
                "Alexander's Ragtime Band",
                "Rainy Days and Mondays",
                "Rhythm Dancer",
                "Ring of Fire",
                "Puttin on the Ritz",
                "Rock Me, Amadeus",
                "Rockin Robin",
                "Rocky Theme",
                "Rule Brittania",
                "Skip to the Loo",
                "Small World",
                "Star Trek Theme",
                "Star Bangled Banner",
                "Star Wars Theme",
                "Surfin Safari",
                "Eye of the Tiger",
                "This Land is Your Land",
                "This Old Man",
                "Those Were the Days",
                "Titanic Theme",
                "Wabash Cannonball",
                "Walkin the Floor Over You",
                "Washington Post March",
                "Whole New World",
                "Yankee Doodle Dandy",
                "Yesterday",
                "Yesterday, When I Was Young",
                "You and Me",
                "Thus Spake Zaruthustra"};
		
		List<String> list = Arrays.asList(names);
		
		ArrayAdapter<String> adapter = new ArrayAdapter<String>(context,android.R.layout.simple_spinner_item,list);
		
		adapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
		
		Spinner spinner = new Spinner(context);
		
		spinner.setAdapter(adapter);

final int[] ids = {R.raw.act_naturally,
           R.raw.africa,
           R.raw.albundy0,
           R.raw.alf,
           R.raw.allofme,
           R.raw.andy_griffith,
           R.raw.another_brick_in_the_wall,
           R.raw.astimegoesby,
           R.raw.allthroughthenight,
           R.raw.alone,
           R.raw.at17,
           R.raw.bad_moon_rising,
           R.raw.ballgame,
           R.raw.beverly_hills_cop,
           R.raw.bingo,
           R.raw.birthday,
           R.raw.blowinginthewind,
           R.raw.breakmystride,
           R.raw.cahereicome1,
           R.raw.caisson,
           R.raw.calledtosayloveyou_2,
           R.raw.charlie_brown,
           R.raw.cheers,
           R.raw.come_on_eileen,
           R.raw.conga,
           R.raw.dallas,
           R.raw.danger_zone,
           R.raw.do_you_remember_these,
           R.raw.enter,
           R.raw.eternalflame,
           R.raw.fame,
           R.raw.final_jeopardy,
           R.raw.flowrsgone,
           R.raw.ghostriders,
           R.raw.grandoldflag,
           R.raw.happytogether,
           R.raw.happytrails,
           R.raw.hawaii_50,
           R.raw.hettywainthropinvestigates,
           R.raw.hill_street_blues,
           R.raw.home,
           R.raw.i_love_lucy,
           R.raw.im_so_excited,
           R.raw.inspirat,
           R.raw.itsallcomingbacktomenow,
           R.raw.jambalaya,
           R.raw.jeopardy,
           R.raw.keepingupappearances,
           R.raw.kingroad,
           R.raw.knots_landing,
           R.raw.lean_on_me,
           R.raw.liosleepstonight,
           R.raw.lone_ranger,
           R.raw.longer,
           R.raw.love_boat,
           R.raw.loveandmarriage,
           R.raw.lucille,
           R.raw.macarena,
           R.raw.marple,
           R.raw.mash,
           R.raw.memories,
           R.raw.mersey,
           R.raw.mission,
           R.raw.mockingbirdsong,
           R.raw.moonriver,
           R.raw.muppets,
           R.raw.nyny,
           R.raw.offwego,
           R.raw.okie_from_muskogee,
           R.raw.oldflame,
           R.raw.oldmac,
           R.raw.paperroses,
           R.raw.perry_mason,
           R.raw.pianoman,
           R.raw.picket_f,
           R.raw.poirot,
           R.raw.popeye,
           R.raw.puff,
           R.raw.quincy,
           R.raw.ragtime,
           R.raw.rainyday,
           R.raw.rhythmdancer,
           R.raw.ring_of_fire,
           R.raw.ritz,
           R.raw.rock_me_amadeus,
           R.raw.rockin_robin_rockinro,
           R.raw.rocky_theme,
           R.raw.rule_britannia,
           R.raw.skip,
           R.raw.smlworld,
           R.raw.star_trek,
           R.raw.starspangledbanner,
           R.raw.starwar,
           R.raw.surfin_safari,
           R.raw.survivors_eyeofthetiger,
           R.raw.thisland,
           R.raw.thisoldman2,
           R.raw.thosewerethedays,
           R.raw.titanic,
           R.raw.wabashcannonballaa,
           R.raw.walkin_the_floor_over_you,
           R.raw.washington_post_march,
           R.raw.whole_new_world,
           R.raw.yankee2,
           R.raw.yesterday,
           R.raw.yesterdaywheniwasyoung,
           R.raw.youme,
           R.raw.zuthra};	
	
	   spinner.setOnItemSelectedListener(new OnItemSelectedListener() {

		@Override
		public void onItemSelected(AdapterView<?> parent, View view,
				int position, long id) {
			// TODO Auto-generated method stub
			
			if (mediaPlayer != null) {
		        mediaPlayer.stop();
		        
		        mediaPlayer = null;
			}
			
			mediaPlayer = MediaPlayer.create(context,ids[position]);
			
			mediaPlayer.start();
		}

		@Override
		public void onNothingSelected(AdapterView<?> parent) {
			// TODO Auto-generated method stub
			
		}
		   
	   });
	   
	   Button button = new Button(context);
	   
	   button.setText("Stop");
	   
	   button.setOnClickListener(new OnClickListener() {

		@Override
		public void onClick(View v) {
			// TODO Auto-generated method stub
			
			if (mediaPlayer != null)
				mediaPlayer.stop();
			
			mediaPlayer = null;
		}
		   
	   });
	   
	   addView(spinner);
	   addView(button);
	}
}
